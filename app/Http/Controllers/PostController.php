<?php

namespace App\Http\Controllers;

use App\Post;
use Auth;
use Illuminate\Support\Facades\Request;

class PostController extends Controller
{
  public function getCreatePost(Request $request)
  {
    return view('createpost');
  }

  public function postCreatePost()
  {
    $user = Auth::id();
    $post = new Post();

    $post->title = Request::get('title');
    $post->user_id = $user;
    $post->save();

    if($post)
    {
      return redirect('/')->with('status', 'Success!');
    }
    else
    {
      return redirect('/')->with('status', 'Failed!');
    }
  }

  public function getEditPost($id)
  {
    $user = Auth::id();
    $post = Post::where('id', $id)->first();

    if($post->user_id == $user)
    {
      return view('editpost',['id' => $id, 'post' => $post]);
    }
    else
    {
      return redirect('/')->with('status', 'You do not have privilege to do this!');
    }
  }

  public function postEditPost($id)
  {
    $user = Auth::id();
    $post = Post::where('id', $id)->first();

    $post->title = Request::get('title');

    if($post->user_id == $user)
    {
      $post->save();
      if($post)
      {
          return redirect('/')->with('status', 'Success!');
      }
      else
      {
          return redirect('/')->with('status', 'Failed!');
      }
    }
    else
    {
      return redirect('/')->with('status', 'Failed!');
    }
  }

  public function getDeletePost($id)
  {
    $user = Auth::id();
    $post = Post::where('id',$id)->first();

    if($post->user_id == $user)
    {
       $post->delete();
       if($post)
       {
         return redirect('/')->with('status', 'Success!');
       }
       else
       {
         return redirect('/')->with('status', 'Failed!');
       }
    }
    else
    {
       return redirect('/')->with('status', 'You do not have privilege to do this!');
    }

  }
}
