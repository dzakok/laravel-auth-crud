@extends('layouts.app')

@section('title')
  Create
@endsection

@section('content')
  <form class="" action="{{ url('createpost') }}" method="post">
    <input type="text" name="title"> </input>
    <button type="submit"> Create </button>
    <input type="hidden" value="{{ Session::token() }}" name="_token">
  </form>
@endsection
